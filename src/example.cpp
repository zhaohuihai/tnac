/*
 * example.cpp
 *
 *  Created on: 2015-8-7
 *      Author: zhaohuihai
 */


#include "TensorOperation/TensorOperation.hpp"

#include "example.h"

using namespace std ;

void test_index()
{
	cout << "test_index" << endl ;
}


void evd_vs_svd()
{
	int D = 4000 ;
	double t_start, t_end ;
	Tensor<double> T(D, D) ;
	T.randUniform() ;
	Tensor<double> A = contractTensors(T, 1, T, 1) ;

	Tensor<double> Z, W ;
	t_start = dsecnd() ;
	symEig(A, Z, W) ;
	t_end = dsecnd() ;
	cout << "EVD time cost: " << ( t_end - t_start ) << endl ;
//	Z.display() ;
	W = W.sort('D') ;
//	W.display() ;

	Tensor<double> U, S, V ;
	t_start = dsecnd() ;
	svd_qr(A, U, S, V) ;
	t_end = dsecnd() ;
	cout << "SVD time cost: " << ( t_end - t_start ) << endl ;
//	U.display() ;
//	V.display() ;
//	S.display() ;

//	Tensor<double> WS = W - S ;
//	cout << WS.maxAbs() << endl ;

}


void test_dsyevr()
{
	int n = 1000 ;

	int m = n ;
	int lda = n ;
	double vl = 0.0 ;
	double vu = 0.0 ;
	int ldz = n ;

	Tensor<int> isuppz(2 * n) ;

	int il = 0 ;
	int iu = 0 ;

	double abstol = 0.0 ; // The absolute error tolerance to which each eigenvalue/eigenvector is required.

	char jobz = 'V' ; // eigenvalues and eigenvectors are computed.
	char range = 'A' ; // the routine computes all eigenvalues.
	char uplo = 'U' ; // a stores the upper triangular part of A.

	Tensor<double> T(n, n) ;
	T.randUniform() ;
//	Tensor<double> A = contractTensors(T, 1, T, 1) ;
//	Tensor<double> A, Z, W ;

//	Tensor<double> Z(n), W(n, n) ;

	// workspace query
//	Tensor<double> work(1) ;
//	int lwork = - 1 ;
//	Tensor<int> iwork(1) ;
//	int liwork = -1 ;

	int info ;

//	dsyevr(&jobz, &range, &uplo, &n, &(A[0]), &lda, &vl, &vu, &il, &iu, &abstol, &m,
//			&(W[0]), &(Z[0]), &ldz, &(isuppz[0]), &(work[0]), &lwork, &(iwork[0]), &liwork, &info) ;

	double a, z, w ;
	double work ;
	int lwork = - 1 ;
	int iwork ;
	int liwork = - 1 ;

	dsyevr(&jobz, &range, &uplo, &n, &(a), &lda, &vl, &vu, &il, &iu, &abstol, &m,
			&(w), &(z), &ldz, &(isuppz[0]), &(work), &lwork, &(iwork), &liwork, &info) ;

	cout << "work size: " << (int)work << endl ;
	cout << "iwork size: " << iwork << endl ;

}

void test_contract()
{
	Tensor<double> A(3, 4, 5, 2) ;
	Tensor<double> B(3, 5, 2, 4) ;
	A.randUniform() ;
	B.randUniform() ;
	Tensor<double> AB ;
	contract(A, "a1, b, c1, d", B, "a,c,d,b", AB, "a1, c1,a,c") ;

	Tensor<double> AB1 = contractTensors(A, 1, 3, B, 3, 2) ;

	AB = AB.permute("a, b, c, d", "b, c, a, d ") ;
	AB1 = AB1.permute(1, 2, 0, 3) ;
	cout << compareTensors(AB, AB1) << endl ;
}
