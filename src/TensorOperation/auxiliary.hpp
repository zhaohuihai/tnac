/*
 * auxiliary.hpp
 *
 *  Created on: 2013-8-19
 *  Updated on: 2014-10-14
 *      Author: ZhaoHuihai
 */

#ifndef AUXILIARY_HPP_
#define AUXILIARY_HPP_

// C++ Standard Template Library
//	1. C library
#include <cmath>
#include <ctime>
#include <cstdlib>
#include <cstdio>
#include <cstdarg>
#include <cassert>
//	2. Containers
#include <vector>
#include <set>
//	3. Input/Output
#include <iostream>
#include <iomanip>
#include <sstream>
#include <fstream>
//	4. Other
#include <algorithm>
#include <string>
#include <complex>
#include <utility>
//=============================================================

inline void makeDir(std::string dirName) ;

template <typename C> std::string num2str(C a) ;
template <typename C> std::string num2str(C a, int precision) ;

// split_string -- Splits a string according to the supplied deliminators
inline std::vector<std::string> split_string(const std::string &str, const std::string &deliminators = " ") ;
// print vector
template <typename C>
void printVector(const std::vector<C>& V, const std::string &vecName, const std::string &deliminators = " ") ;

template <typename C> void disp(C a) ;

inline int mod(int x, int y) ;

template <typename C> std::vector<C> vec(const C& a0) ;
template <typename C> std::vector<C> vec(const C& a0, const C& a1) ;
template <typename C> std::vector<C> vec(const C& a0, const C& a1, const C& a2) ;
template <typename C> std::vector<C> vec(const C& a0, const C& a1, const C& a2, const C& a3) ;
template <typename C> std::vector<C> vec(const C& a0, const C& a1, const C& a2, const C& a3, const C& a4) ;
template <typename C> std::vector<C> vec(const C& a0, const C& a1, const C& a2, const C& a3, const C& a4, const C& a5) ;
template <typename C> std::vector<C> vec(const C& a0, const C& a1, const C& a2, const C& a3, const C& a4,
																				 const C& a5, const C& a6) ;
template <typename C> std::vector<C> vec(const C& a0, const C& a1, const C& a2, const C& a3, const C& a4,
																				 const C& a5, const C& a6, const C& a7) ;
template <typename C> std::vector<C> vec(const C& a0, const C& a1, const C& a2, const C& a3, const C& a4,
																				 const C& a5, const C& a6, const C& a7, const C& a8) ;
//===========================================================

void makeDir(std::string dirName)
{
	std::string makeDirectory = "mkdir -p " ;
	makeDirectory = makeDirectory + dirName ;
	system( &(makeDirectory[0]) ) ;
}

template <typename C>
std::string num2str(C a)
{
	std::ostringstream ss ;
	ss << a ;
	return ss.str() ;

}

template <typename C>
std::string num2str(C a, int precision)
{
	std::ostringstream ss ;
	ss << std::setprecision(precision) << a ;
	return ss.str() ;
}

/// Splits a string into pieces using the supplied deliminators.
/// str           The string to be split.
/// deliminators  Characters used to deliminate the sections the string is to be split into.
/// \return A vector holding the sections of the split string.
std::vector<std::string> split_string(const std::string &str, const std::string &deliminators)
{
	// Initialize the return value
	std::vector<std::string> retval ;

  // create a set of the deliminators
  std::set<char> delim_set;
  for (int i = 0; i < deliminators.size(); i++)
    delim_set.insert(deliminators.at(i));

  // Loop through the string searching for deliminating characters
  int i1 = 0;
  for (int i2 = 0; i2 < str.size(); i2++)
    // When the deliminating character is found, add a new string
    // to the return value consisting of all characters between the
    // previous and current deliminating characters
    if (delim_set.count(str.at(i2)))
    {
      if (i2 > i1)
        retval.push_back(str.substr(i1, i2-i1));
      i1 = i2 + 1;
    }

  // Add a string to the return value consisting of any characters between
  // the last deliminator and the end of the string
  if (i1 < str.size())
    retval.push_back(str.substr(i1, str.size()-i1));

  // return the vector of the split string
  return retval;
}

template <typename C>
void printVector(const std::vector<C>& V, const std::string &vecName, const std::string &deliminators)
{
	std::cout << vecName << std::endl ;
	for ( int i = 0 ; i < V.size(); i ++ ) std::cout << V[i] << deliminators ;
	std::cout << std::endl ;
}

int mod(int x, int y)
{
	int r = x % y ;
	if (r < 0)
	{
		r = r + y ;
	}
	return r ;
}

template <typename C>
std::vector<C> vec(const C& a0)
{
	std::vector<C> A ;
	A.push_back(a0) ;
	return A ;
}

template <typename C>
std::vector<C> vec(const C& a0, const C& a1)
{
	std::vector<C> A ;
	A.push_back(a0) ;
	A.push_back(a1) ;
	return A ;
}

template <typename C>
std::vector<C> vec(const C& a0, const C& a1, const C& a2)
{
	std::vector<C> A ;
	A.push_back(a0) ;
	A.push_back(a1) ;
	A.push_back(a2) ;
	return A ;
}

template <typename C>
std::vector<C> vec(const C& a0, const C& a1, const C& a2, const C& a3)
{
	std::vector<C> A ;
	A.push_back(a0) ;
	A.push_back(a1) ;
	A.push_back(a2) ;
	A.push_back(a3) ;
	return A ;
}

template <typename C>
std::vector<C> vec(const C& a0, const C& a1, const C& a2, const C& a3, const C& a4)
{
	std::vector<C> A ;
	A.push_back(a0) ;
	A.push_back(a1) ;
	A.push_back(a2) ;
	A.push_back(a3) ;
	A.push_back(a4) ;
	return A ;
}

template <typename C>
std::vector<C> vec(const C& a0, const C& a1, const C& a2, const C& a3, const C& a4, const C& a5)
{
	std::vector<C> A ;
	A.push_back(a0) ;
	A.push_back(a1) ;
	A.push_back(a2) ;
	A.push_back(a3) ;
	A.push_back(a4) ;
	A.push_back(a5) ;
	return A ;
}

template <typename C>
std::vector<C> vec(const C& a0, const C& a1, const C& a2, const C& a3, const C& a4,
									 const C& a5, const C& a6)
{
	std::vector<C> A ;
	A.push_back(a0) ;
	A.push_back(a1) ;
	A.push_back(a2) ;
	A.push_back(a3) ;
	A.push_back(a4) ;
	A.push_back(a5) ;
	A.push_back(a6) ;
	return A ;
}

template <typename C>
std::vector<C> vec(const C& a0, const C& a1, const C& a2, const C& a3, const C& a4,
									 const C& a5, const C& a6, const C& a7)
{
	std::vector<C> A ;
	A.push_back(a0) ;
	A.push_back(a1) ;
	A.push_back(a2) ;
	A.push_back(a3) ;
	A.push_back(a4) ;
	A.push_back(a5) ;
	A.push_back(a6) ;
	A.push_back(a7) ;
	return A ;
}

#endif /* AUXILIARY_HPP_ */
