/*
 * blas_lapack.hpp
 *
 *  Created on: 2015-7-13
 *      Author: zhaohuihai
 */

#ifndef BLAS_LAPACK_HPP_
#define BLAS_LAPACK_HPP_

#include <vector>
#include <mkl.h>

//============================== BLAS ==============================

// y = x
inline void xcopy(const MKL_INT n, const double *x, double *y)
{
	int incx = 1 ;
	int incy = 1 ;
	dcopy(&n, x, &incx, y, &incy) ;
}

inline void xcopy(const MKL_INT n, const std::vector<double>& x, std::vector<double>& y)
{
	int incx = 1 ;
	int incy = 1 ;
	dcopy(&n, &(x[0]), &incx, &(y[0]), &incy) ;
}

// x = a*x
inline void xscal(const MKL_INT n, const double a, double *x)
{
	int incx = 1 ;
	dscal(&n, &a, x, &incx) ;
}

// vector addition. y = a*x + y
inline void xaxpy(const MKL_INT n, const double a, const double *x, const MKL_INT incx,
									double *y, const MKL_INT incy)
{
	daxpy(&n, &a, x, &incx, y, &incy) ;
}
inline void xaxpy(const MKL_INT n, const double a, const double *x, double *y)
{
	int incx = 1 ;
	int incy = 1 ;
	daxpy(&n, &a, x, &incx, y, &incy) ;
}

// res = sum{i}_[x(i)*y(i)]
inline double xdot(const MKL_INT n, const double *x, const double *y)
{
	int incx = 1 ;
	int incy = 1 ;
	return ddot(&n, x, &incx, y, &incy) ;
}

// y = x + y
inline void xaxpby(const MKL_INT n, const std::vector<double>& x, std::vector<double>& y)
{
	int incx = 1 ;
	int incy = 1 ;
	double a = 1.0 ;
	double b = 1.0 ;
	daxpby(&n, &a, &(x[0]), &incx, &b, &(y[0]), &incy) ;
}

// Computes a matrix-vector product using a general matrix
// y := alpha*A*x + beta*y or y := alpha*A'*x + beta*y
inline void xgemv(char trans, const MKL_INT m, const MKL_INT n,
									const double alpha, const double *a, const double *x,
									const double beta, double *y)
{
	int lda = m ;
	int incx = 1 ;
	int incy = 1 ;
	dgemv(&trans, &m, &n, &alpha, a, &lda, x, &incx, &beta, y, &incy) ;
}
// y := a*x or y := a'*x
inline void xgemv(char trans, const MKL_INT m, const MKL_INT n, const double *a, const double *x, double *y)
{
	int lda = m ;
	int incx = 1 ;
	int incy = 1 ;
	double alpha = 1.0 ;
	double beta = 0.0 ;
	dgemv(&trans, &m, &n, &alpha, a, &lda, x, &incx, &beta, y, &incy) ;
}

inline void xgemv(char trans, const MKL_INT m, const MKL_INT n,
									const std::vector<double>& a, const std::vector<double>& x, std::vector<double>& y)
{
	int lda = m ;
	int incx = 1 ;
	int incy = 1 ;
	double alpha = 1.0 ;
	double beta = 0.0 ;
	dgemv(&trans, &m, &n, &alpha, &(a[0]), &lda, &(x[0]), &incx, &beta, &(y[0]), &incy) ;
}

//============================== LAPACK ==============================

inline int xsyevr(int n, double* a, double* w, double* z, int* isuppz, double* work, int* iwork)
{
	char jobz = 'V' ; // eigenvalues and eigenvectors are computed.
	char range = 'A' ; // the routine computes all eigenvalues.
	char uplo = 'U' ; // a stores the upper triangular part of A.

	int lda = n ;

	double vl = 0.0 ;
	double vu = 0.0 ;

	int il = 0 ;
	int iu = 0 ;

	double abstol = 0.0 ; // The absolute error tolerance to which each eigenvalue/eigenvector is required.

	int m = n ;
	int ldz = n ;

	int lwork = 26 * n ;
	int liwork = 10 * n ;
	int info ;

	dsyevr(&jobz, &range, &uplo, &n, a, &lda, &vl, &vu, &il, &iu, &abstol, &m,
			   w, z, &ldz, isuppz, work, &lwork, iwork, &liwork, &info) ;

	return info ;
}

inline int xsyevr(int n, std::vector<double>& a, std::vector<double>& w, std::vector<double>& z,
		              std::vector<int>& isuppz, std::vector<double>& work, std::vector<int>& iwork)
{
	char jobz = 'V' ; // eigenvalues and eigenvectors are computed.
	char range = 'A' ; // the routine computes all eigenvalues.
	char uplo = 'U' ; // a stores the upper triangular part of A.

	int lda = n ;

	double vl = 0.0 ;
	double vu = 0.0 ;

	int il = 0 ;
	int iu = 0 ;

	double abstol = 0.0 ; // The absolute error tolerance to which each eigenvalue/eigenvector is required.

	int m = n ;
	int ldz = n ;

	int lwork = work.size() ;
	int liwork = iwork.size() ;
	int info ;

	dsyevr(&jobz, &range, &uplo, &n, &(a[0]), &lda, &vl, &vu, &il, &iu, &abstol, &m,
			   &(w[0]), &(z[0]), &ldz, &(isuppz[0]), &(work[0]), &lwork, &(iwork[0]), &liwork, &info) ;

	return info ;
}

#endif /* BLAS_LAPACK_HPP_ */
