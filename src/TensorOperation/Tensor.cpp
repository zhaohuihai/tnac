/*
 * Tensor.cpp
 *
 *  Created on: 2015-10-13
 *      Author: zhaohuihai
 */


#include "Tensor.h"

using namespace std ;

double compareTensors(Tensor<double>& A, Tensor<double>& A0)
{
	if (!isAllDimAgree(A, A0))
	{
		std::cout << "compareTensors error: input two tensors are not the same dimension." << std::endl ;
		std::cout << "they are not comparable." << std::endl ;
		exit(0) ;
	}
	else
	{
		Tensor<double> Adiff = A - A0 ;
		double c = Adiff.maxAbs() / A.maxAbs() ;
		return c ;
	}
}

// contract(A, "a, b", B, "b, c", AB, "a, c")
void contract(Tensor<double>& A, std::string  Ainds,
							Tensor<double>& B, std::string  Binds,
							Tensor<double>& AB, std::string ABinds)
{
	// split indices strings
	vector< string > Ainds_vec = split_string(Ainds, ", ") ;
	vector< string > Binds_vec = split_string(Binds, ", ") ;
	vector< string > ABinds_vec = split_string(ABinds, ", ") ;
	// check whether number of indices agree with tensor rank
	if ( A.rank() != Ainds_vec.size() || B.rank() != Binds_vec.size() ) {
		cout << "contract error: tensor Index must be defined with correct length" << endl ;
		exit(0) ;
	}

	vector<int> Aind_num_right, Bind_num_left ; // common indices serial number
	// find common indices serial number,and output indices string
	vector<string> ABinds_vec_1 = findCommonIndex(Ainds_vec, Aind_num_right, Binds_vec, Bind_num_left) ;
	// check whether ABinds is correct
	for ( int i = 0; i < ABinds_vec.size(); i ++) {
		if ( ABinds_vec[i] != ABinds_vec_1[i] ) {
			cout << "contract error: output tensor indices are not correct." << endl ;
			printVector(ABinds_vec, "user defined indices", ", ") ;
			printVector(ABinds_vec_1, "resulting indices", ", ") ;
			exit(0) ;
		}
	}

//	printVector(Aind_num_right, "Aright", ", ") ;
//	printVector(Bind_num_left, "Bleft", ", ") ;
//	printVector(ABinds_vec_1, "AB", ", ") ;

	contract(A, Aind_num_right, B, Bind_num_left, AB) ;
}

// find common indices serial number,and output indices string
vector<string> findCommonIndex(std::vector<std::string> Ainds_vec, std::vector<int> &Aind_num_right,
															 std::vector<std::string> Binds_vec, std::vector<int> &Bind_num_left)
{
	Aind_num_right.clear() ;
	Bind_num_left.clear() ;
	vector<string> indsCommon ;

	for ( int i = 0; i < Ainds_vec.size(); i ++ ) {
		for ( int j = 0 ; j < Binds_vec.size(); j ++ ) {
			if ( Ainds_vec[i] == Binds_vec[j] ) {
				Aind_num_right.push_back(i) ;
				Bind_num_left.push_back(j) ;
				indsCommon.push_back(Ainds_vec[i]) ;
				break ;
			}
		}
	}
	//-------------------------------------------------
	vector< string > ABinds_vec ;
	for ( int i = 0; i < Ainds_vec.size(); i ++ ) {
		bool existence = false ;
		for ( int j = 0; j < indsCommon.size(); j ++ ) {
			if ( Ainds_vec[i] == indsCommon[j] ) {
				existence = true ;
				break ;
			}
		}
		if ( !existence ) ABinds_vec.push_back(Ainds_vec[i]) ;
	}

	for ( int i = 0 ; i < Binds_vec.size(); i ++ ) {
		bool existence = false ;
		for ( int j = 0 ; j < indsCommon.size(); j ++ ) {
			if ( Binds_vec[i] == indsCommon[j] ) {
				existence = true ;
				break ;
			}
		}
		if ( !existence ) ABinds_vec.push_back(Binds_vec[i]) ;
	}
	return ABinds_vec ;
}

void contract(Tensor<double>& A, std::vector<int>& Aind_num_right,
							Tensor<double>& B, std::vector<int>& Bind_num_left,
							Tensor<double>& AB)
{
	Tensor<int> Aind_right(Aind_num_right.size()) ;
	Tensor<int> Bind_left(Bind_num_left.size()) ;
	for ( int i = 0; i < Aind_num_right.size(); i ++ ) {
		Aind_right[i] = Aind_num_right[i] ;
		Bind_left[i] = Bind_num_left[i] ;
	}
	AB = contractTensors(A, Aind_right, B, Bind_left) ;
}


//-------------------------------------------with Lapack------------------------------------------------

// Computes eigenvalues and eigenvectors of a real symmetric matrix using the Relatively Robust Representations.
// A * Z = Z * W ,
// Z: the columns of Z are the right eigenvectors.
// W: W is a column vector which contains the eigenvalue in increasing order.
int symEig(Tensor<double> A, Tensor<double>& Z, Tensor<double>& W)
{
	if (A.rank() != 2)
	{
		std::cout << "symEig error: 1st input must be a matrix." << std::endl ;
		exit(0) ;
	}
	if (A.dimension(0) != A.dimension(1))
	{
		std::cout << "symEig error: 1st input matrix must be square." << std::endl ;
		exit(0) ;
	}

	char jobz = 'V' ; // eigenvalues and eigenvectors are computed.
	char range = 'A' ; // the routine computes all eigenvalues.
	char uplo = 'U' ; // a stores the upper triangular part of A.

	int n = A.dimension(0) ; // The order of the matrix A.

	int lda = n ;

	double vl = 0.0 ;
	double vu = 0.0 ;

	int il = 0 ;
	int iu = 0 ;

	double abstol = 0.0 ; // The absolute error tolerance to which each eigenvalue/eigenvector is required.

	int m = n ;

	W = Tensor<double>(n) ;
	Z = Tensor<double>(n, n) ;

	int ldz = n ;

	Tensor<int> isuppz(2 * n) ;

	//************************************************
	// workspace query
	Tensor<double> work(1) ;
	int lwork = - 1 ;
	Tensor<int> iwork(1) ;
	int liwork = -1 ;

	int info ;

	dsyevr(&jobz, &range, &uplo, &n, &(A[0]), &lda, &vl, &vu, &il, &iu, &abstol, &m,
			&(W[0]), &(Z[0]), &ldz, &(isuppz[0]), &(work[0]), &lwork, &(iwork[0]), &liwork, &info) ;

	//********************************************************************
	// computation
	lwork = (int)work(0) ;
	work = Tensor<double>(lwork) ;
	liwork = iwork(0) ;
	iwork = Tensor<int>(liwork) ;

	dsyevr(&jobz, &range, &uplo, &n, &(A[0]), &lda, &vl, &vu, &il, &iu, &abstol, &m,
			&(W[0]), &(Z[0]), &ldz, &(isuppz[0]), &(work[0]), &lwork, &(iwork[0]), &liwork, &info) ;

	if (info != 0)
	{
		std::cout << "info = " << info << std::endl ;
	}

	return info ;
}

// Computes the eigenvalues and left and right eigenvectors of a real non-symmetric matrix.
// input:
// A (real non-symmetric matrix)
// output:
// Wr: column vector which contains real part of eigenvalues
// Wi: column vector which contains imaginary part of eigenvalues
// Vl: left eigenvectors are stored in the columns, complex conjugate pair are Vl(:,j)+i*Vl(:,j+1) and Vl(:,j)-i*Vl(:,j+1)
// Vr: right eigenvectors are stored in the columns, complex conjugate pair are Vr(:,j)+i*Vr(:,j+1) and Vr(:,j)-i*Vr(:,j+1)
int reNonSymEig(Tensor<double> A, Tensor<double> &Wr, Tensor<double> &Wi, Tensor<double> &Vl, Tensor<double> &Vr)
{
	if (A.rank() != 2)
	{
		std::cout << "reNonSymEig error: 1st input must be a matrix." << std::endl ;
		exit(0) ;
	}
	if (A.dimension(0) != A.dimension(1))
	{
		std::cout << "reNonSymEig error: 1st input matrix must be square." << std::endl ;
		exit(0) ;
	}

	char jobvl = 'V' ; // left eigenvectors of A are computed.
	char jobvr = 'V' ; // right eigenvectors of A are computed.

	int n = A.dimension(0) ; // The order of the matrix A (n �����ゆ�烽����ゆ�烽��濮�锟斤拷������锟芥�ゆ�烽��浠�锟斤拷���锟� 0)

	int lda = n ; // The leading dimension of the array a. Must be at least max(1, n).
	// The leading dimensions of the output arrays vl and vr
	int ldvl = n ;
	int ldvr = n ;
	/*
	 *  Contain the real and imaginary parts, respectively, of the computed eigenvalues.
	 *  Complex conjugate pairs of eigenvalues appear consecutively
	 *  with the eigenvalue having positive imaginary part first.
	 */
	Wr = Tensor<double>(n) ;
	Wi = Tensor<double>(n) ;
	/*
	 *  left eigenvectors are stored in the columns,
	 *  complex conjugate pair are Vl(:,j)+i*Vl(:,j+1) and Vl(:,j)-i*Vl(:,j+1)
	 */
	Vl = Tensor<double>(n, n) ;
	/*
	 *  right eigenvectors are stored in the columns,
	 *  complex conjugate pair are Vr(:,j)+i*Vr(:,j+1) and Vr(:,j)-i*Vr(:,j+1)
	 */
	Vr = Tensor<double>(n, n) ;

	int info ; //
	//***************************************************
	// workspace query
	Tensor<double> work(1) ;
	int lwork = - 1 ;

	dgeev(&jobvl, &jobvr, &n, &(A[0]), &lda, &(Wr[0]), &(Wi[0]), &(Vl[0]), &ldvl, &(Vr[0]), &ldvr, &(work[0]), &lwork, &info) ;

	//***************************************************
	// computation
	lwork = (int)work(0) ;
	work = Tensor<double>(lwork) ;

	dgeev(&jobvl, &jobvr, &n, &(A[0]), &lda, &(Wr[0]), &(Wi[0]), &(Vl[0]), &ldvl, &(Vr[0]), &ldvr, &(work[0]), &lwork, &info) ;

	if (info != 0)
	{
		std::cout << "info = " << info << std::endl ;
	}
	//**************************************************
	return info ;
}

// Computes the singular value decomposition of a general rectangular matrix using a divide and conquer method.
// A = U * S * V'
// info = svd(A, U, S, V) ;
int svd(Tensor<double> A, Tensor<double>& U, Tensor<double>& S, Tensor<double>& V)
{
	if (A.rank() != 2)
	{
		std::cout << "svd error: 1st input must be a matrix." << std::endl ;
		exit(0) ;
	}

	char jobz = 'S' ;

	int m = A.dimension(0) ; // The number of rows of the matrix A
	int n = A.dimension(1) ; // The number of columns in A

	int lda = m ; // The leading dimension of  A
	int ldu = m ; // The leading dimensions of U
	int ldvt = std::min(m, n) ; // The leading dimensions of VT

	S = Tensor<double>(ldvt) ;
	U = Tensor<double>(m, ldvt) ;
	V = Tensor<double>(ldvt, n) ;

	Tensor<int> iwork(8 * ldvt) ;

	int info ;
	//************************************************
	// workspace query
	Tensor<double> work(1) ;
	int lwork = - 1 ;

	dgesdd(&jobz, &m, &n, &(A[0]), &lda, &(S[0]), &(U[0]), &ldu, &(V[0]), &ldvt, &(work[0]), &lwork, &(iwork[0]), &info) ;

	//*********************************************************************
	// computation
	lwork = (int)work(0) ;
	work = Tensor<double>(lwork) ;

	dgesdd(&jobz, &m, &n, &(A[0]), &lda, &(S[0]), &(U[0]), &ldu, &(V[0]), &ldvt, &(work[0]), &lwork, &(iwork[0]), &info) ;

//	double t_start = dsecnd() ;
	V = V.trans() ;
//	double t_end = dsecnd() ;
//	std::cout << "trans() time cost: " << (t_end - t_start) << std::endl ;
	//
	if (info != 0)
	{
		std::cout << "svd error info = " << info << std::endl ;
	}

	return info ;
}

// Computes the singular value decomposition of a general rectangular matrix using QR method
// A = U * S * V'
// info = svd_qr(A, U, S, V) ;
int svd_qr(Tensor<double> A, Tensor<double>& U, Tensor<double>& S, Tensor<double>& V)
{
	if (A.rank() != 2)
	{
		std::cout << "svd_qr error: 1st input must be a matrix." << std::endl ;
		exit(0) ;
	}

	char jobu = 'S' ;
	char jobvt = 'S' ;
	int m = A.dimension(0) ; // The number of rows of the matrix A
	int n = A.dimension(1) ; // The number of columns in A
	int lda = m ; // The leading dimension of  A
	int ldu = m ; // The leading dimensions of U
	int ldvt = std::min(m, n) ; // The leading dimensions of VT

	S = Tensor<double>(ldvt) ;
	U = Tensor<double>(m, ldvt) ;
	V = Tensor<double>(ldvt, n) ;

	int info ;
	//************************************************
	// workspace query
	Tensor<double> work(1) ;
	int lwork = - 1 ;

	dgesvd(&jobu, &jobvt, &m, &n, &(A[0]), &lda, &(S[0]), &(U[0]), &ldu, &(V[0]), &ldvt, &(work[0]), &lwork, &info) ;

	//*********************************************************************
	// computation
	lwork = (int)work(0) + 1 ;
	work = Tensor<double>(lwork) ;

	dgesvd(&jobu, &jobvt, &m, &n, &(A[0]), &lda, &(S[0]), &(U[0]), &ldu, &(V[0]), &ldvt, &(work[0]), &lwork, &info) ;

	V = V.trans() ;

	if (info != 0)
	{
		std::cout << "svd_qr error info = " << info << std::endl ;
	}

	return info ;
}

// Computes the QR factorization of a general m-by-n matrix.
// A = Q * R
int qr(Tensor<double>& A, Tensor<double>& R)
{
	if (A.rank() != 2)
	{
		std::cout << "qr error: 1st input must be a matrix." << std::endl ;
		exit(0) ;
	}

	int m = A.dimension(0) ; // The number of rows in the matrix A
	int n = A.dimension(1) ; // The number of columns in A

	int k = std::min(m, n) ;

	R = A ;

	int lda = m ;

	Tensor<double> tau(k) ;

	int info ;
	//************************************************
	// workspace query
	Tensor<double> work(1) ;
	int lwork = - 1 ;

	dgeqrf(&m, &n, &(R[0]), &lda, &(tau[0]), &(work[0]), &lwork, &info) ;

	//************************************************
	// computation
	lwork = (int)work(0) ;
	work = Tensor<double>(lwork) ;

	dgeqrf(&m, &n, &(R[0]), &lda, &(tau[0]), &(work[0]), &lwork, &info) ;

	if (info != 0)
	{
		std::cout << "info = " << info << std::endl ;
	}

	if ( m > n)
	{
		R = R.subTensor(0,n-1, 0,n-1) ;
	}

	for (int i = 0; i < R.dimension(0); i ++)
	{
		for (int j = 0; j < i ; j ++)
		{
			R(i, j) = 0 ;
		}
	}
	return info ;
}

// LQ factorization
// Computes the LQ factorization of a general m-by-n matrix.
// A = L * Q
int lq(Tensor<double>& A, Tensor<double>& L)
{
	if (A.rank() != 2)
	{
		std::cout << "lq error: 1st input must be a matrix." << std::endl ;
		exit(0) ;
	}

	int m = A.dimension(0) ; // The number of rows in the matrix A
	int n = A.dimension(1) ; // The number of columns in A

	int k = std::min(m, n) ;

	L = A ;

	int lda = m ;

	Tensor<double> tau(k) ;

	int info ;
	//************************************************
	// workspace query
	Tensor<double> work(1) ;
	int lwork = - 1 ;

	dgelqf(&m, &n, &(L[0]), &lda, &(tau[0]), &(work[0]), &lwork, &info) ;

	//************************************************
	// computation
	lwork = (int)work(0) ;
	work = Tensor<double>(lwork) ;

	dgelqf(&m, &n, &(L[0]), &lda, &(tau[0]), &(work[0]), &lwork, &info) ;

	if (info != 0)
	{
		std::cout << "info = " << info << std::endl ;
	}

	if ( m < n)
	{
		L = L.subTensor(0,m-1, 0,m-1) ;
	}
	// upper triangular part is set to zero
	for (int i = 0; i < L.dimension(0); i ++)
	{
		for (int j = (i + 1); j < L.dimension(1) ; j ++)
		{
			L(i, j) = 0 ;
		}
	}

	return info ;
}
//***********************************************************************

