/*
 * parameter.h
 *
 *  Created on: 2011-5-31
 *      Author: zhaohuihai
 */

#ifndef PARAMETER_H_
#define PARAMETER_H_

#define Default        0
#define EPS            2.22044604925031e-16

//===============================================================================
// hierarchy of line symbols:
// 1. ======================================================
// 2. +++++++++++++++++++++++++++++++++++++++++++++++++++++
// 3. ---------------------------------------------------
// 4. **************************************************
// 5. /////////////////////////////////////////////////
// 6. %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
// 7. :::::::::::::::::::::::::::::::::::::::::::::::
// 8. ##############################################
//===============================================================================

class Parameter
{
private:

public:
	//constructor
	Parameter() ;
	Parameter(const int chi, const int RGsteps, const double temp) ;
	//destructor
	~Parameter() ;
	// coupling const.
	double J ;
	/// square lattice Ising model temperature
	double temp ;
	int chi ;
	int RG_method ; // 1: TRG, 2: SRG
	double RG_minVal ;
	// lattice size: 8*3^RGsteps
	int RGsteps ;
	///+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	/// define debugging parameters
	// display intermediate result or not
	bool disp ;
};
//==================================================================================

#endif /* PARAMETER_H_ */
