/*
 * example.h
 *
 *  Created on: 2015-8-7
 *      Author: zhaohuihai
 */

#ifndef SRC_EXAMPLE_H_
#define SRC_EXAMPLE_H_

void test_index() ;

void evd_vs_svd() ;

void test_dsyevr() ;

void test_contract() ;


#endif /* SRC_EXAMPLE_H_ */
