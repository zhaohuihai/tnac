/*
 * parameter.cpp
 *
 *  Created on: 2011-6-9
 *      Author: zhaohuihai
 */

// C++ Standard Template Library
//	1. C library
#include <stdlib.h>
#include <cmath>
#include <iostream>

#include "parameter.h"

//using namespace std ;

Parameter::Parameter()
{
	// coupling const.
	// AFM : J = 1; FM: J = -1
	J = - 1 ;
	/// square lattice Ising model temperature
	temp = 2.0/log(1.0 + sqrt(2.0)) ;
	chi = 10 ;
	RG_method = 1; // 1: TRG, 2: SRG
	RG_minVal = 1e-20 ;
	// lattice size: 8*3^RGsteps
	RGsteps =10;
	///+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	/// define debugging parameters
	// display intermediate result or not
	disp = false ;
}

Parameter::Parameter(const int _chi, const int _RGsteps, const double _temp) {
	// coupling const.
	// AFM : J = 1; FM: J = -1
	J = - 1 ;
	/// square lattice Ising model temperature
	temp = _temp ;
	chi = _chi ;
	RG_method = 2; // 1: TRG, 2: SRG
	RG_minVal = 1e-30 ;
	// lattice size: 8*3^RGsteps
	RGsteps = _RGsteps;
	///+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	/// define debugging parameters
	// display intermediate result or not
	disp = false ;
}

//============================================================================

Parameter::~Parameter() { }
