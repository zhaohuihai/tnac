/*
 * environment_honeycomb.h
 *
 *  Created on: 2014-10-2
 *      Author: ZhaoHuihai
 */

#ifndef ENVIRONMENT_HONEYCOMB_H_
#define ENVIRONMENT_HONEYCOMB_H_

#include "TensorOperation/TensorOperation.hpp"
#include "parameter.h"

class EnvironmentHoneycomb
{
private:
	int _RGsteps ;
	int _chi ;

public:
	// constructor
	EnvironmentHoneycomb() ;

	EnvironmentHoneycomb(Parameter &parameter, TensorArray<double> &T) ;
	// destructor
	~EnvironmentHoneycomb() ;
	//-----------------------------------------------------------------

	double minVal ;

	int RG_method ;

	// environment tensors
	TensorArray<double> Re, Te ;
	// Singular values on every bond
	TensorArray<double> SVs ;
	// RG factor of every scale
	Tensor<double> RGfactor ;
	//-----------------------------------------------------------------
	int chi() { return _chi ; }
	void TRG() ; //u
	void TRG(int RGscale) ;
	void TRG(int startScale, int Nscale) ;

	void renormalizeTtoR(Tensor<double> &T0, Tensor<double> &T1,
												 Tensor<double> &Rz0, Tensor<double> &Rz1, double& truncErr) ; //u
	void renormalizeTtoR(Tensor<double> &T0, Tensor<double> &T1,
												 Tensor<double> &Rz0, Tensor<double> &Rz1) ;

	void truncateSVD(Tensor<double> &U, Tensor<double> &S, Tensor<double> &V) ;
	void truncateSVD(Tensor<double> &U, Tensor<double> &S, Tensor<double> &V, double& truncErr) ;
	int getChi(Tensor<double>& S) ;

	double contract3RtoT(Tensor<double> &Rz, Tensor<double> &Rx, Tensor<double> &Ry, Tensor<double> &T) ; //u

	Tensor<double> compute_Menv() ;
	Tensor<double> compute_initial_env() ; // only T, no R
	void lower_scale_env(Tensor<double> &M, int RGscale) ;

	Tensor<double> compute_Menv(int RGscale) ; //u

	Tensor<double> compute_Mz_01(int RGscale) ;
	Tensor<double> Mz_01_sites_24() ;
	Tensor<double> Mz_01_sites_72() ;

	double computeBond(Parameter& parameter, Tensor<double> &Menv,
				TensorArray<double> &T, TensorArray<double> &Th) ;

	double computeSite(Parameter& parameter, TensorArray<double> &A,
										 TensorArray<double> &T,
										 Tensor<double> &Menv, Tensor<double> Op0, Tensor<double> Op1) ;

	void truncateSmall(Tensor<double>& U, Tensor<double>& S, Tensor<double>& V) ;
	void truncateSmall(Tensor<double>& U, Tensor<double>& S, Tensor<double>& V, double minSing) ;

	Tensor<double> createTh(Tensor<double> &A, Tensor<double> U) ;
	Tensor<double> createTh_site(Tensor<double> &A, Tensor<double> h) ;
	//-----------------------------------------------------------------------------------------------
	void SRGfinite() ;

	double SRG(int RGscale) ;
	double SRGsimple(int RGscale) ;
	double SRGinf(int RGscale) ;

	void SRG_TtoR_old(int RGscale, Tensor<double>& T0,   Tensor<double>& T1,
			Tensor<double>& Menv, Tensor<double>& SVz0, Tensor<double>& SVz1,
			Tensor<double>& Rz0,  Tensor<double>& Rz1, double& truncErr) ;
	void shift_singVal(Tensor<double>& S) ;

	void biorthogonal(int RGscale, Tensor<double>& rho,
			Tensor<double>& PR, Tensor<double>& PL,
			Tensor<double>& SV0, Tensor<double>& SV1, double& truncErr) ;
	void symBondDenMat(int RGscale, Tensor<double>& rho,
			Tensor<double>& PR, Tensor<double>& PL,
			Tensor<double>& SV0, Tensor<double>& SV1, double& truncErr) ;
	double optimize_P(Tensor<double>& RR, Tensor<double>& LL,
			Tensor<double>& P) ;
	double optimize_Q(Tensor<double>& RR, Tensor<double>& LL,
			Tensor<double>& P) ;

	double optimize_isoP(Tensor<double>& rho, Tensor<double>& P, Tensor<double>& Q) ;
	double optimize_isoQ(Tensor<double>& rho, Tensor<double>& P, Tensor<double>& Q) ;

	void truncateSymEig(Tensor<double>& U, Tensor<double>& L, double& truncErr) ;

	double compareVecs_diff(TensorArray<double>& V, TensorArray<double>& V0) ;

	void rotate() ;

	//-----------------------------------------------------------------------------------------------
	double freeEnergy() ;
};



#endif /* ENVIRONMENT_HONEYCOMB_H_ */
